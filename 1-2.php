<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form method="POST">
        <input type="text" name="firstNumber" required>
        <input type="text" name="secondNumber" required>
        <button name="add" type="submit">+</button>
        <button name="subtract" type="submit">-</button>
        <button name="divide" type="submit">/</button>
        <button name="multiply" type="submit">*</button>
    </form>
<?php
if ((isset($_POST['firstNumber'])) && (isset($_POST['secondNumber']))) {
    $firstNumber = $_POST['firstNumber'];
    $secondNumber = $_POST['secondNumber'];
    if (isset($_POST['add'])) {
        echo $firstNumber + $secondNumber;
    } elseif (isset($_POST['subtract'])) {
        echo $firstNumber - $secondNumber;
    } elseif (isset($_POST['divide'])) {
        echo $firstNumber / $secondNumber;
    } elseif (isset($_POST['multiply'])) {
        echo $firstNumber * $secondNumber;
    } else {
        echo "choose an operation";
    }
}
?>
</body>
</html>