<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show the greatest common divisor.</title>
</head>
<body>
    <h1>Show the greatest common divisor.</h1>
    <form method="POST">
        <input type="text" name="firstNumber" required>
        <input type="text" name="secondNumber" required>
        <button type="submit" name="submit">submit</button>
    </form>
<?php
function gcd($firstNumber, $secondNumber)
{
    if ($secondNumber == 0) {
        return $firstNumber;
    }
    return gcd($secondNumber, ($firstNumber % $secondNumber));
}

if (isset($_POST['submit'])) {
    $firstNumber = $_POST['firstNumber'];
    $secondNumber = $_POST['secondNumber'];
    echo 'The GCD is: ' . gcd($firstNumber, $secondNumber);
}
?>
</body>
</html>


