<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solve FizzBuzz problem.</title>
</head>
<body>
    <h1>
        Solve FizzBuzz problem.
    </h1>
    <form method="POST">
    <input type="text" name="number" required>
    <button type="submit">
        submit
    </button>
</form>
<?php
if (isset($_POST['number']) && ($_POST['number'] !== null)) {
    $number = $_POST['number'];
    for ($i = 1; $i <= $number; $i++) {
        if ($i % 3 == 0 && $i % 5 == 0) {
            echo 'FizzBuzz';
        } elseif ($i % 3 == 0) {
            echo 'Fizz';
        } elseif ($i % 5 == 0) {
            echo 'Buzz';
        } else {
            echo $i;
        }
        echo '<br>';
    }
}
?>
</body>
</html>