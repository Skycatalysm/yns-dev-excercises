<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show 3 days from inputted date</title>
</head>
<body>
    <h3>Input date. Then show 3 days from inputted date and its day of the week.</h3>
    <form method="POST">
        <input type="date" name="date">
        <button type="submit">submit</button>
    </form>
<?php
if (isset($_POST['date'])) {
    $date = $_POST['date'];
    $date = date('Y-m-d l', strtotime($date . '+3 days'));
    echo $date;
}
?>
</body>
</html>