<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Registration</title>
</head>
<body>
    <h2>
        User Registration
    </h2>
    <form action="1-6_to_1-13_result.php" method="POST" enctype="multipart/form-data">

        <b>User Name:</b><br>
        <input type="text" name="userName" required><br>

        <b>Password:</b><br>
        <input type="password" name="password" required><br>

        <b>First Name:</b><br>
        <input type="text" name="firstName" required><br>

        <b>Middle Name:</b><br>
        <input type="text" name="middleName" required><br>
        
        <b>Last Name:</b><br>
        <input type="text" name="lastName" required><br>

        <b>Email:</b><br>
        <input type="email" name="email" required><br>

        <b>Age:</b><br>
        <input type="number" name="age" required><br>

        <b>Img:</b><br>
        <input type="file" name="fileToUpload" required><br><br>

        <button type="submit" name="submit">Submit</button>
    </form>
</body>
</html>