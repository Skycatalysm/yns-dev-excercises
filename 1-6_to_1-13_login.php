<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['userSession']) && !empty($_SESSION['userSession'])) {
    echo '<b style="font-size:20px;"> Hello ' . $_SESSION['userSession']['firstName'] . '</b> ';
    echo '<a style="float:right" href="1-6_to_1-13_logout.php">Logout</a>';
    echo '<br>';
    echo 'Here is your information:<br><br>';
    echo 'Profile Picture:<br> <img style="max-width: 100%;height: 10rem;" src="uploads/' . $_SESSION['userSession']['img'] . '" alt="profile picture"><br>';
    echo 'First Name: ' . $_SESSION['userSession']['firstName'] . '<br>';
    echo 'Middle Name: ' . $_SESSION['userSession']['middleName'] . '<br>';
    echo 'Last Name: ' . $_SESSION['userSession']['lastName'] . '<br>';
    echo 'Age: ' . $_SESSION['userSession']['age'] . '<br>';
    echo 'Email: ' . $_SESSION['userSession']['email'] . '<br>';
} else {
    ?>
    <form method="POST">
        <b>User Name</b>
        <input type="text" name="userName">

        <b>Password</b>
        <input type="password" name="password">
        <input type="submit" name="submit">
    </form>
    <?php
}
if (isset($_POST['submit'])) {
    $userName = $_POST['userName'];
    $password = $_POST['password'];

    if (empty($userName)) {
        echo 'Please enter a username<br>';
    }

    if (empty($password)) {
        echo 'Please enter a password<br>';
    }

    $file = fopen("user_info.csv", "r");
    while (!feof($file)) {
        $list[] = fgetcsv($file);
    }

    foreach ($list as $key) {
        if (($userName == $key[5]) && (md5($password) == $key[6])) {
            echo 'success';
            $_SESSION['userSession']['firstName'] = $key[0];
            $_SESSION['userSession']['middleName'] = $key[1];
            $_SESSION['userSession']['lastName'] = $key[2];
            $_SESSION['userSession']['email'] = $key[3];
            $_SESSION['userSession']['age'] = $key[4];
            $_SESSION['userSession']['img'] = $key[7];
            header("Refresh:0");
        }
    }
}
?>
</body>
</html>