<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Result</title>
</head>
<body>
<?php
//check if button is submitted
if (isset($_POST['submit'])) {
    ///Check for the submitted values
    isset($_POST['firstName']) ? $firstName = $_POST['firstName'] : $firstName = null;
    isset($_POST['middleName']) ? $middleName = $_POST['middleName'] : $middleName = null;
    isset($_POST['lastName']) ? $lastName = $_POST['lastName'] : $lastName = null;
    isset($_POST['email']) ? $email = $_POST['email'] : $email = null;
    isset($_POST['age']) ? $age = $_POST['age'] : $age = null;
    isset($_POST['userName']) ? $userName = $_POST['userName'] : $userName = null;
    (isset($_POST['password']) && ($_POST['password'] != "")) ? $password = md5($_POST['password']) : $password = null;

    //Create a variable that will be filled with arrays later
    $userInfo = [];

    //Check First Name
    if ($firstName == null) {
        echo '<h4>Please provide a first name<h4>';
        $userInfo[] = 'null';
    } else {
        if (ctype_alpha($firstName)) {
            /**
             * put First Name as 1st array
             */
            $userInfo[] = $firstName;
        } else {
            $userInfo[] = 'null';
            echo "<div style='background-color:red'>First Name: $firstName should only contain alphabet</div>";
        }
    }

    //Check Middle Name
    if ($middleName == null) {
        echo '<h4>Please provide a middle name<h4>';
        $userInfo[] = 'null';
    } else {
        if (ctype_alpha($middleName)) {
            /**
             * put Middle Name as 2nd array
             */
            $userInfo[] = $middleName;
        } else {
            $userInfo[] = 'null';
            echo "<div style='background-color:red'>Middle Name: $middleName should only contain alphabet</div>";
        }
    }

    //Check Last Name
    if ($lastName == null) {
        echo "<h4>Please provide a last name<h4>";
        $userInfo[] = "null";
    } else {
        if (ctype_alpha($lastName)) {
            /**
             * put Last Name as 3rd array
             */
            $userInfo[] = $lastName;
        } else {
            $userInfo[] = "null";
            echo "<div style='background-color:red'>Last Name: $lastName should only contain alphabet</div>";
        }
    }

    //Check Email
    if ($email == null) {
        echo '<h4>Please provide an email<h4>';
        $userInfo[] = "null";
    } else {
        $regex = '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/';
        if (preg_match($regex, $email)) {
            /**
             * put Email as 4th array
             */
            $userInfo[] = $email;
        } else {
            $userInfo[] = 'null';
            echo "<div style='background-color:red'>Email: $email is not valid</div>";
        }
    }

    //Check age
    if ($age == null) {
        echo '<h4>Please provide an age<h4>';
        $userInfo[] = 'null';
    } else {
        if (ctype_digit($age)) {
            /**
             * put Age as 5th array
             */
            $userInfo[] = $age;
        } else {
            $userInfo[] = 'null';
            echo "<div style='background-color:red'>Age: $age should be a number</div>";
        }
    }

    //Check User Name
    if ($userName == null) {
        echo '<h4>Please provide User Name<h4>';
        $userInfo[] = 'null';
    } else {
        /**
         * put User name as 6th array
         */
        $userInfo[] = $userName;
    }

    //Check Password
    if ($password == null) {
        echo '<h4>Please provide a password<h4>';
        $userInfo[] = 'null';
    } else {
        /**
         * put Password as 7th array
         */
        $userInfo[] = $password;
    }

    //Uploading of image
    $targetDirectory = "uploads/";
    $extension = pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION);
    $imgName = uniqid("img", true) . ".$extension";
    $targetFile = $targetDirectory . $imgName;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            echo 'File is not an image.';
            $uploadOk = 0;
        }
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" && $imageFileType != "jfif") {
        echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo 'Sorry, your file was not uploaded.';
        $userInfo[] = 'null';
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
            $userInfo[] = $imgName;
        } else {
            echo 'Sorry, there was an error uploading your file.';
            array_pop($userInfo);
            $userInfo[] = 'null';
        }
    }

    //Check each info if it is submitted correctly
    foreach ($userInfo as $item) {
        if ($item == "null") {
            $userStatus = 0;
            break;
        }
        $userStatus = 1;
    }

    //Check if the status is ok, then save
    if ($userStatus == 1) {

        /**
         * put user information to csv
         */
        $file = fopen("user_info.csv", "r");
        while (!feof($file)) {
            $list[] = fgetcsv($file);
        }
        fclose($file);
        array_push($list, $userInfo);
        $file = fopen("user_info.csv", "w");
        foreach (array_filter($list) as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
        echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
        echo "File is an image - " . $check["mime"] . ".";
        echo "<h3>Registered Successfully!</h3>";
    } else {
        echo "<h3>Failed. Please fix the data given</h3>";
    }

}
?>
</body>
</html>
