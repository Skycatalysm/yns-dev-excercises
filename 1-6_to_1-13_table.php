<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<?php
//Count the rows of data inside the csv
$rowCount = count(file('user_info.csv'));

//Divide the result from the csv row count for the paging
if (($rowCount % 10) == 0) {
    $rowCount = $rowCount / 10;
} else {
    $rowCount = $rowCount / 10;
    floor($rowCount);
    $rowCount++;
}

//Open and fetch the data for the page
$file = fopen("user_info.csv", "r");
$page = isset($_GET['p']) ? $_GET['p'] : 1;
$page = $page * 10;
$page = $page - 9;
$row = 1;
$list = [];
while (!feof($file)) {
    if ($row < $page) {
        fgetcsv($file);
    } else {
        $list[] = fgetcsv($file);
    }
    $row++;
    if ($row == $page + 10) {
        break;
    }
}
fclose($file);
?>
<table border="1">
  <tr>
    <th align="center" colspan=6">User Info</th>
  </tr>
  <tr>
    <th align="center">Profile Picture</th>
    <th align="center">First Name</th>
    <th align="center">Middle Name</th>
    <th align="center">Last Name</th>
    <th align="center">Email</th>
    <th align="center">Age</th>
  </tr>
<?php
//Output the user data inside the tr with each user info inside it's td
foreach (array_filter($list) as $info) {
    echo '<tr>';
    if ($info[7] == 'null') {
        echo '<td align="center">no profile picture</td>';
    } else {
        echo "<td align='center'><img style='max-width: 100%;height: 10rem;' src='uploads/$info[7]' alt='profile picture'></td>";
    }

    //Remove the three confidential data of user
    array_pop($info);
    array_pop($info);
    array_pop($info);

    foreach ($info as $item) {
        echo "<td>$item</td>";
    }
    echo "</tr>";
}
//Output the navigation for the pager
echo "<tr> <td align='center' colspan='6'>";
for ($i = 1; $i <= $rowCount; $i++) {
    echo "<a href='?p=$i'> $i </a>&nbsp;";
}
echo "</td></tr>";
?>
</table>
</body>
</html>

