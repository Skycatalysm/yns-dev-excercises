<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scroll screen when you press buttons.</title>
</head>
<body style="height:200vh;position:relative;text-align:center;">
    <button id="scrollDown" style="position:absolute;top:0;">Scroll Down</button>
    <button id="scrollTop" style="position:absolute;bottom:0;">Scroll Up</button>

    <script src="js/2-10.js"></script>
</body>
</html>