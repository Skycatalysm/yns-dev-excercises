<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show another image</title>
</head>
<body style="text-align:center;">
    <img style="height:20rem;max-width: 100%" src="uploads/p2.jpg" alt="person" id="image">

    <script src="js/2-12.js"></script>
</body>
</html>