<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Change size of images when you press buttons.</title>
</head>
<body style="text-align:center;">
    <div id="buttons" style="margin-bottom:5px;">    
        <button id="turnToSmall">Small</button>
        <button id="turnToMedium">Medium</button>
        <button id="turnToLarge">Large</button>
    </div>
    <img style="height:20rem;max-width: 100%" src="uploads/pikachu.jpg" alt="person" id="image">
    <script src="js/2-13.js"></script>
</body>
</html>