<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show images according to the options in combo box.</title>
</head>
<body style="text-align:center;">
    <img style="height:20rem;max-width: 100%" src="uploads/bacon_egg.jfif" alt="food" id="image">
    <br>
    <select id="foods">
        <option value="bacon_egg.jfif">Bacon and Egg</option>
        <option value="pizza.jpg">Pizza</option>
        <option value="burger.jfif">Burger</option>
    </select>

    <script src="js/2-14.js"></script>
</body>
</html>