<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show alert when you click an image.</title>
</head>
<body>
    <div style="text-align: center;">
        <h3>Click the image!</h3>
        <img src="uploads/slime.jpg" alt="slime" id="image">
    </div>

    <script src="js/2-7.js"></script>
</body>
</html>