<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Change text and background color when you press buttons.</title>
</head>
<body style="background-color: white;">
    <div style="text-align: center;">
        <h3 id="colorName">White!</h3>
        <button id="redButton">Red</button>
        <button id="blueButton">Blue</button>
        <button id="greenButton">Green</button>
        <button id="yellowButton">Yellow</button>
    </div>
    <script src="js/2-9.js"></script>
</body>
</html>