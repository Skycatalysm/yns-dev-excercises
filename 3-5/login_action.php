<?php
include_once 'config.php';

if (isset($_POST['submit'])) {
    $userName = $_POST['userName'];
    $password = $_POST['password'];

    if (empty($userName)) {
        $_SESSION['errorMessage'] = 'Please enter a username<br>';
        header("Location: ./");
        die();
    }

    if (empty($password)) {
        $_SESSION['errorMessage'] = 'Please enter a password<br>';
        header("Location: ./");
        die();

    }

    $password = md5($password);

    //Check if user exists
    $sql = "SELECT * FROM `user_info` WHERE username = '$userName' AND password = '$password';";

    $result = $connect->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        $userInfo = $result->fetch_assoc();

        $_SESSION['userSession']['firstName'] = $userInfo['first_name'];
        $_SESSION['userSession']['middleName'] = $userInfo['middle_name'];
        $_SESSION['userSession']['lastName'] = $userInfo['last_name'];
        $_SESSION['userSession']['email'] = $userInfo['email'];
        $_SESSION['userSession']['age'] = $userInfo['age'];
        $_SESSION['userSession']['img'] = $userInfo['image'];

        header("Location: profile.php");
    } else {
        echo $_SESSION['errorMessage'] = 'It\'s seems like your account doesn\'t exist. <br>';
        header("Location: ./");
        die();

    }
}
