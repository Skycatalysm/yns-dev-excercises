-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2019 at 04:15 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `small_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Exective'),
(2, 'Admin'),
(3, 'Sales'),
(4, 'Development'),
(5, 'Design'),
(6, 'Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `department_id` int(255) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `boss_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `first_name`, `last_name`, `middle_name`, `birth_date`, `department_id`, `hire_date`, `boss_id`) VALUES
(1, 'Manabu', 'Yamazaki', NULL, '1976-03-15', 1, NULL, NULL),
(2, 'Tomohiko', 'Takasago', NULL, '1974-05-24', 3, '2014-04-01', 1),
(3, 'Yuta', 'Kawakami', NULL, '1990-08-13', 4, '2014-04-01', 1),
(4, 'Shogo', 'Kubota', NULL, '1985-01-31', 4, '2014-12-01', 1),
(5, 'Lorraine ', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
(6, 'Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
(7, 'Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
(8, 'Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
(9, 'Hideshi', 'Ogoshi', NULL, '1983-07-15', 4, '2014-06-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_positions`
--

CREATE TABLE `employee_positions` (
  `id` int(11) NOT NULL,
  `employee_id` int(255) NOT NULL,
  `position_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_positions`
--

INSERT INTO `employee_positions` (`id`, `employee_id`, `position_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 3, 5),
(6, 4, 5),
(7, 5, 5),
(8, 6, 5),
(9, 7, 5),
(10, 8, 5),
(11, 9, 5),
(12, 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`) VALUES
(1, 'CEO'),
(2, 'CTO'),
(3, 'CFO'),
(4, 'Manager'),
(5, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `age` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `first_name`, `middle_name`, `last_name`, `email`, `age`, `username`, `password`, `image`) VALUES
(13, 'Brian', 'David', 'Tabuada', 'skycatalysm@gmail.com', 12, 'sky', '827ccb0eea8a706c4c34a16891f84e7b', 'img5d833cdeaae496.09303146.jpg'),
(14, 'dummy', 'dummy', 'dummy', 'dumm@gmail.com', 12, 'dummy', '275876e34cf609db118f3d84b799a790', 'img5d8347f972d325.68212067.jfif'),
(15, 'dummya', 'dummya', 'dummya', 'daumm@gmail.com', 12, 'second', '827ccb0eea8a706c4c34a16891f84e7b', 'img5d8348145bbc53.95769778.jfif'),
(16, 'dummyas', 'dummyas', 'dummyas', 'daumms@gmail.com', 12, 'third', '827ccb0eea8a706c4c34a16891f84e7b', 'img5d8348277c90c4.93323814.jfif'),
(17, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(18, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(19, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(20, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(21, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(22, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(23, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(24, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(25, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(26, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(27, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(28, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(29, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(30, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(31, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(32, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(33, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(34, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(35, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(36, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(37, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(38, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(39, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(40, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(41, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(42, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(43, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(44, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(45, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(46, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(47, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(48, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(49, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(50, 'test', 'test', 'test', 'test@gmail.com', 7, '', 'test', 'img5d842d405043a3.49190648.jpg'),
(51, 'newacc', 'newacc', 'newacc', 'newacc@gmail.com', 12, 'newacc', '80228804a380482e66e256f340e9ede8', 'img5d842d405043a3.49190648.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_positions`
--
ALTER TABLE `employee_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `employee_positions`
--
ALTER TABLE `employee_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
