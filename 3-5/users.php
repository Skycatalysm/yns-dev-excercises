<?php
include_once 'head.php';

if (!isset($_SESSION['userSession']['firstName'])) {
    echo '<script src="js/strict_login.js"></script> ';
}

//start of body
?>
<div class="container">
    <div class="text-center my-5">
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">ID</th>
                <th scope="col">Profile Picture</th>
                <th scope="col">First Name</th>
                <th scope="col">Middle Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Email</th>
                <th scope="col">Age</th>
                </tr>
            </thead>
            <tbody>
<?php
//Checks if pagination exists. If not put a default value
$page = isset($_GET['p']) ? $_GET['p'] : 1;
$pageStart = ($page * 10) - 10;

//Create query
$sql = "SELECT * FROM `user_info` LIMIT 10 OFFSET $pageStart;";

$result = $connect->query($sql);

//Fetch all users
if ($result->num_rows > 0) {
    // output data of each row
    while ($userInfo = mysqli_fetch_assoc($result)) {
        echo '<tr>';
        echo '<td>' . $userInfo['id'] . '</td>';
        echo '<td><img class="rounded-circle" src="uploads/' . $userInfo['image'] . '" alt="profile picture" width="140" height="140"></td>';
        echo '<td>' . $userInfo['first_name'] . '</td>';
        echo '<td>' . $userInfo['middle_name'] . '</td>';
        echo '<td>' . $userInfo['last_name'] . '</td>';
        echo '<td>' . $userInfo['email'] . '</td>';
        echo '<td>' . $userInfo['age'] . '</td>';
        echo '</tr>';
    }
}
?>
            </tbody>
        </table>
        <nav class="float-right">
            <ul class="pagination">
<?php
$sql = "SELECT count(id) FROM `user_info`;";
$result = $connect->query($sql);

if ($result->num_rows > 0) {
    $rowCount = mysqli_fetch_assoc($result);
    $rowCount = $rowCount['count(id)'];

    //Divide the result from the row count for the paging
    if (($rowCount % 10) == 0) {
        $rowCount = $rowCount / 10;
    } else {
        $rowCount = $rowCount / 10;
        floor($rowCount);
        $rowCount++;

    }

    for ($i = 1; $i <= $rowCount; $i++) {
        echo "<li class='page-item'><a class='page-link' href='?p=$i'>$i</a></li>";
    }

}
?>
            </ul>
        </nav>
    </div>
</div>
<?php
//end of body
include_once 'footer.php';
?>