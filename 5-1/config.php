<?php
session_start();

$serverName = 'localhost';
$userName = 'root';
$password = '';
$dbname = "question_storage";

// Create connection
$connect = new mysqli($serverName, $userName, $password, $dbname);

// Check connection
if ($connect->connect_error) {
    die('Connection failed: ' . $connect->connect_error);
}

