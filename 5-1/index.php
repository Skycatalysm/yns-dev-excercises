<?php include_once 'config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="width: 50rem;margin:auto;">
        <h1 style="text-align:center">Random Question Generator</h1>
        <?php 
        if(isset($_SESSION['score'])){
            echo '<h3 style="padding:10px 0px; text-align:center;background-color:#117A65;color:#fff"> Your score is: '
            .$_SESSION['score'].
            '</h3>';
            unset($_SESSION['score']);
        } 
        ?>
        <form action="check_answers.php" method="POST">
            <?php
            $sql = "SELECT questions.id AS id, questions.question AS question, GROUP_CONCAT(choices.value separator ',') AS choices, GROUP_CONCAT(choices.id separator ',') AS choices_id FROM questions LEFT JOIN choices ON questions.id = choices.question_id GROUP BY questions.id ORDER BY RAND() LIMIT 10;";
            $result = $connect->query($sql);
            if ($result->num_rows > 0) {
                // output data of each row
                while ($questions = mysqli_fetch_assoc($result)) {
                    $id = $questions['id'];
                    $question = $questions['question'];
                    $choices = explode(',', $questions['choices']);
                    $choices_id = explode(',', $questions['choices_id']);        
                    echo '<div style="margin: 50px 0px;">';
                    echo $question;
                    echo '<div style="float:right;">';
                    for ($i = 0; $i < count($choices); $i++) {
                        echo '<input type="radio" name="' . $id . '" value="' . $choices_id[$i] . '"> ' .$choices[$i];
                    }
                    echo '</div>';
                    echo "<hr/>";
                    echo '</div>';
                }
            }
            ?>
            <button type="submit" name="submit">Submit!</button>
        </form>
    </div>
</body>
</html>
