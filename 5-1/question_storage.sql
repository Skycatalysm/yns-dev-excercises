-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 03:26 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `question_storage`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` int(255) NOT NULL,
  `choices_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `question_id`, `choices_id`) VALUES
(1, 2, 2),
(5, 3, 4),
(6, 4, 9),
(7, 5, 11),
(8, 28, 14),
(9, 29, 18),
(10, 30, 21),
(11, 31, 22),
(12, 32, 25),
(13, 33, 29),
(14, 34, 31),
(15, 35, 36),
(16, 36, 38),
(17, 37, 40),
(18, 38, 45),
(19, 39, 47),
(20, 40, 50),
(21, 41, 52),
(22, 42, 55),
(23, 43, 59);

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `id` int(255) NOT NULL,
  `question_id` int(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `question_id`, `value`) VALUES
(1, 2, '1'),
(2, 2, '2'),
(3, 2, '3'),
(4, 3, '3'),
(5, 3, '4'),
(6, 3, '5'),
(7, 4, '7'),
(8, 4, '8'),
(9, 4, '4'),
(10, 5, '2'),
(11, 5, '5'),
(12, 5, '8'),
(13, 28, '7'),
(14, 28, '6'),
(15, 28, '5'),
(16, 29, '22'),
(17, 29, '129'),
(18, 29, '7'),
(19, 30, '30'),
(20, 30, '4'),
(21, 30, '8'),
(22, 31, '9'),
(23, 31, '10'),
(24, 31, '11'),
(25, 32, '10'),
(26, 32, '12'),
(27, 32, '33'),
(28, 33, '77'),
(29, 33, '11'),
(30, 33, '13'),
(31, 34, '1'),
(32, 34, '10'),
(33, 34, '100'),
(34, 35, '22'),
(35, 35, '200'),
(36, 35, '2'),
(37, 36, '23'),
(38, 36, '3'),
(39, 36, '300'),
(40, 37, '4'),
(41, 37, '5'),
(42, 37, '400'),
(43, 38, '2'),
(44, 38, '6'),
(45, 38, '5'),
(46, 39, '20'),
(47, 39, '6'),
(48, 39, '7'),
(49, 40, '60'),
(50, 40, '7'),
(51, 40, '33'),
(52, 41, '8'),
(53, 41, '12'),
(54, 41, '100'),
(55, 42, '9'),
(56, 42, '12'),
(57, 42, '43'),
(58, 43, '1'),
(59, 43, '10'),
(60, 43, '100');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(255) NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`) VALUES
(2, 'What is the result of 1+1?'),
(3, 'What is the result of 1+2?'),
(4, 'What is the result of 1+3?'),
(5, 'What is the result of 1+4?'),
(28, 'What is the result of 1+5?'),
(29, 'What is the result of 1+6?'),
(30, 'What is the result of 1+7?'),
(31, 'What is the result of 1+8?'),
(32, 'What is the result of 1+9?'),
(33, 'What is the result of 1+10?'),
(34, 'What is the result of 1*1?'),
(35, 'What is the result of 1*2?'),
(36, 'What is the result of 1*3?'),
(37, 'What is the result of 1*4?'),
(38, 'What is the result of 1*5?'),
(39, 'What is the result of 1*6?'),
(40, 'What is the result of 1*7?'),
(41, 'What is the result of 1*8?'),
(42, 'What is the result of 1*9?'),
(43, 'What is the result of 1*10?');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `choices_id` (`choices_id`);

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `answer_ibfk_2` FOREIGN KEY (`choices_id`) REFERENCES `choices` (`id`);

--
-- Constraints for table `choices`
--
ALTER TABLE `choices`
  ADD CONSTRAINT `choices_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
