<?php

class CalendarDate
{
    //Calculates the previous calendar button
    public function previousDate(int $month, int $year): string
    {
        if ($month <= 1) {
            $previousMonth = 12;
            $previousYear = $year - 1;
            return "?m=$previousMonth&y=$previousYear";
        }
        $previousMonth = $month - 1;
        $previousYear = $year;
        return "?m=$previousMonth&y=$year";
    }

    //Calculates the next calendar button
    public function nextDate(int $month, int $year): string
    {
        if ($month >= 12) {
            $previousMonth = 1;
            $previousYear = $year + 1;
            return "?m=$previousMonth&y=$previousYear";
        }
        $previousMonth = $month + 1;
        $previousYear = $year;
        return "?m=$previousMonth&y=$year";
    }

    //Put to arrays the calculated days
    public function calculateDays(int $month, int $year): array
    {
        $dayToday = $this->checkDateToday($month, $year);

        $number = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateArr = array();
        for ($i = 1; $i <= $number; $i++) {
            if ($i === (int) $dayToday) {
                $dateArr[] = '<div class="grid-item active">' . $i . '</div>';
                continue;
            }
            $dateArr[] = '<div class="grid-item">' . $i . '</div>';
        }
        return $dateArr;
    }

    //Check if the current year and month is being shown
    public function checkDateToday(int $month, int $year)
    {
        $yearToday = date("Y");
        $monthToday = date("m");

        if (($month === (int) $monthToday) && ($year === (int) $yearToday)) {
            return date("d");
        }
        return null;

    }

    //Returns the month name based on the number given
    public function returnMonthName($month): string
    {
        $dateObj = DateTime::createFromFormat('!m', $month);
        return $dateObj->format('F');
    }
}
