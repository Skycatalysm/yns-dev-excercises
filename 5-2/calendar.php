<?php
require_once 'DateClass.php';
$calendarDate = new CalendarDate();
if (isset($_GET['y']) && isset($_GET['m']) && (($_GET['m'] <= 12) && ($_GET['m'] >= 1)) && (!ctype_alpha($_GET['y']))) {
    $getMonth = $_GET['m'];
    $getYear = $_GET['y'];
} else {
    $getMonth = date("m");
    $getYear = date("Y");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/calendar_css.css">
</head>
<body>
    <h1 class="title"><?php echo $calendarDate->returnMonthName($getMonth).' '.$getYear; ?></h1>
    <div class="grid-container">
        <?php
        $daysArray = $calendarDate->calculateDays($getMonth, $getYear);
        foreach ($daysArray as $day) {
            echo $day;
        }
        ?>
    </div>
    <div class="grid-container centered" style="">
        <button class="btn" onclick="location.href='<?php echo $calendarDate->previousDate($getMonth, $getYear) ?> ';">Previous</button>
        <button class="btn" onclick="location.href='<?php echo $calendarDate->nextDate($getMonth, $getYear) ?> ';">Next</button>
    </div>
</body>
</html>
