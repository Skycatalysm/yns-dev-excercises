<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/nav.css">
</head>
<body>
    <h1 class="title">Navigation</h1>

    <div class="vertical-menu">
        <a href="../1-1.php">[1-1]Show Hello World.</a>
        <a href="../1-2.php">[1-2]The four basic operations of arithmetic.</a>
        <a href="../1-3.php">[1-3]Show the greatest common divisor.</a>
        <a href="../1-4.php">[1-4]Solve FizzBuzz problem.</a>
        <a href="../1-5.php">[1-5]Input date. Then show 3 days from inputted date and its day of the week.</a>
        <a href="../3-5">[1-6 to 1-13]One small system divided into smaller tasks.</a>
        <a href="../1-6_to_1-13.php">[1-6]Input user information. Then show it in next page.</a>
        <a href="../1-6_to_1-13.php">[1-7]Add validation in the user information form(required, numeric, character, mail address).</a>
        <a href="../1-6_to_1-13.php">[1-8]Store inputted user information into a CSV file.</a>
        <a href="../1-6_to_1-13_table.php">[1-9]Show the user information using table tags.</a>
        <a href="../1-6_to_1-13.php">[1-10]Upload images.</a>
        <a href="../1-6_to_1-13_table.php">[1-11]Show uploaded images in the table.</a>
        <a href="../1-6_to_1-13_table.php">[1-12]Add pagination in the list page.</a>
        <a href="../1-6_to_1-13_login.php">[1-13]Create login form and embed it into the system that you developed.</a>
        <a href="../2-1.php">[2-1]Show alert.</a>
        <a href="../2-2.php">[2-2]Confirm dialog and redirection</a>
        <a href="../2-3.php">[2-3]The four basic operations of arithmetic</a>
        <a href="../2-4.php">[2-4]Show prime numbers.</a>
        <a href="../2-5.php">[2-5]Input characters in text box and show it in label.</a>
        <a href="../2-6.php">[2-6]Press button and add a label below button.</a>
        <a href="../2-7.php">[2-7]Show alert when you click an image.</a>
        <a href="../2-8.php">[2-8]Show alert when you click link.</a>
        <a href="../2-9.php">[2-9]Change text and background color when you press buttons.</a>
        <a href="../2-10.php">[2-10]Scroll screen when you press buttons.</a>
        <a href="../2-11.php">[2-11]Change background color using animation.</a>
        <a href="../2-12.php">[2-12]Show another image when you mouse over an image. Then show the original image when you mouse out.</a>
        <a href="../2-13.php">[2-13]Change size of images when you press buttons.</a>
        <a href="../2-14.php">[2-14]Show images according to the options in combo box.</a>
        <a href="../2-15.php">[2-15]Show current date and time in real time.</a>
        <a href="../sql_files">[3-1 to 3-5]SQL files.</a>
        <a href="../3-5">[3-5]Use database in the applications that you developed.</a>
        <a href="../linux_vagrant">[4-1 to 4-8]* Linux / Vagrant.</a>
        <a href="../5-1">[5-1]Quiz with three multiple choices</a>
        <a href="../5-2/calendar.php">[5-2]Calendar</a>
        <a href="../5-3">[5-3]Navigation</a>
    </div>
</body>
</html>
