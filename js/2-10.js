//Get the scrolling element
var scrollingElement = (document.scrollingElement || document.body);

//get the button id to trigger scroll down on click
var scrollDown = document.getElementById('scrollDown')
scrollDown.onclick = function () {
    scrollToBottom();
}

function scrollToBottom() {
    scrollingElement.scrollTop = scrollingElement.scrollHeight;
}

//get the button id to trigger scroll top on click
var scrollTop = document.getElementById('scrollTop')
scrollTop.onclick = function () {
    scrollToTop();
}

function scrollToTop(id) {
    scrollingElement.scrollTop = 0;
}