var image = document.getElementById('image');

var turnToSmall = document.getElementById('turnToSmall');
turnToSmall.onclick = function () {
    image.style.height = "20rem";
}

var turnToMedium = document.getElementById('turnToMedium');
turnToMedium.onclick = function () {
    image.style.height = "40rem";
}

var turnToLarge = document.getElementById('turnToLarge');
turnToLarge.onclick = function () {
    image.style.height = "50rem";
}