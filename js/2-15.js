var second = 1000;


function updateClock() {
    var time = document.getElementById('time'), newDate = new Date();
    time.innerHTML = newDate.getDate() + '/' + (parseInt(newDate.getMonth()) + 1) + '/' + newDate.getFullYear() + ' ' + newDate.getHours() + ':' + newDate.getMinutes();
}

setInterval(updateClock, second);