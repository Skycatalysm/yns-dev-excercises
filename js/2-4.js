var submit = document.getElementById('submit');
submit.onclick = function () {
    primeNumbers()
};

function primeNumbers() {
    var number = document.getElementById('number');
    var result = gcd(number.value);
    alert("The GCD is: " + result);
}

function gcd(firstNumber, secondNumber = 2) {
    if (secondNumber == 0) {
        return firstNumber;
    }
    return gcd(secondNumber, (firstNumber % secondNumber));
}