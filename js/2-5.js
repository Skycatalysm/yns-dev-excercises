var input = document.getElementById('input');
input.oninput = function () {
    updateLabel()
};

function updateLabel() {
    var output = document.getElementById('output');
    output.innerHTML = input.value;
}