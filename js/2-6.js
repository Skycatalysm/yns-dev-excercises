var addLabel = document.getElementById('addLabel');
addLabel.onclick = function () {
    addNewLabel();
}

function addNewLabel() {
    var parent = document.body
    var node = document.createElement("LABEL");
    var textNode = document.createTextNode("New Label");
    node.appendChild(textNode);
    parent.insertBefore(node, addLabel.nextSibling);
}