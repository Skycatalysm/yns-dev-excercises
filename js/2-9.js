//Get the color name
var colorName = document.getElementById('colorName');

//get the id of a button
var redButton = document.getElementById('redButton');
//onclick of the button change background and the inner html of h3
redButton.onclick = function () {
    document.body.style.backgroundColor = "red";
    colorName.innerHTML = "Red!";
}

//get the id of a button
var blueButton = document.getElementById('blueButton');
//onclick of the button change background and the inner html of h3
blueButton.onclick = function () {
    document.body.style.backgroundColor = "blue";
    colorName.innerHTML = "Blue!";
}

//get the id of a button
var greenButton = document.getElementById('greenButton');
//onclick of the button change background and the inner html of h3
greenButton.onclick = function () {
    document.body.style.backgroundColor = "green";
    colorName.innerHTML = "Green!";
}

//get the id of a button
var yellowButton = document.getElementById('yellowButton');
//onclick of the button change background and the inner html of h3
yellowButton.onclick = function () {
    document.body.style.backgroundColor = "yellow";
    colorName.innerHTML = "Yellow!";
}