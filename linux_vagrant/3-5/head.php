<?php
include_once 'config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Simple System</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <b class="navbar-brand">Simple System</b>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <?php
      if (isset($_SESSION['userSession']['firstName'])) {
          echo '
            <li class="nav-item">
                <a class="nav-link text-uppercase font-weight-bold" href="users.php">list of users</a>
            </li>';
          echo '
            <li class="nav-item">
                <a class="nav-link text-uppercase font-weight-bold" href="profile.php">profile</a>
            </li>';
      }
      ?>
    </ul>
  <?php
  if (isset($_SESSION['userSession']['firstName'])) {
      echo '<a href="logout.php" class="btn btn-light">Logout</a>';
  } else {
      echo '
        <form action="login_action.php" method="POST" class="form-inline my-2 my-lg-0">
        <input name="userName" class="form-control mr-sm-2" type="text" placeholder="User Name" required>
        <input name="password" class="form-control mr-sm-2" type="password" placeholder="Password" required>
        <button name="submit" class="btn btn-secondary my-2 my-sm-0" type="submit">Login</button>
      </form>
    ';
  }
  ?>
  </div>
</nav>
<?php
if (isset($_SESSION['errorMessage'])) {
    echo '<div class="alert alert-danger">' . $_SESSION['errorMessage'] . '</div>';
    unset($_SESSION['errorMessage']);
}
?>
