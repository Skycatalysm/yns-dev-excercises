<?php
include_once 'head.php';
//start of body
?>
<form action="register_action.php" class="m-5 bg-light border border-dark p-3 rounded" method="POST" enctype="multipart/form-data">
  <fieldset>
    <legend>Registration</legend>
    <div class="row">
        <div class="form-group col-6">
        <label for="userName">User Name</label>
        <input name="userName" type="text" class="form-control" id="userName" placeholder="Enter desired user name">
        </div>
        <div class="form-group col-6">
        <label for="password">Password</label>
        <input name="password" type="password" class="form-control" id="password" placeholder="Enter desired password">
        <small class="form-text text-muted">Please be creative with your your password.</small>
        </div>
        <div class="form-group col-6">
        <label for="firstName">First Name</label>
        <input name="firstName" type="text" class="form-control" id="firstName" placeholder="Please enter your first name">
        </div>
        <div class="form-group col-6">
        <label for="middleName">Middle Name</label>
        <input name="middleName" type="text" class="form-control" id="middleName" placeholder="Please enter your middle name">
        </div>
        <div class="form-group col-6">
        <label for="lastName">Last Name</label>
        <input name="lastName" type="text" class="form-control" id="lastName" placeholder="Please enter your last name">
        </div>
        <div class="form-group col-6">
        <label for="email">Email</label>
        <input name="email" type="email" class="form-control" id="email" placeholder="Please enter your email">
        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group col-6">
        <label for="age">Age</label>
        <input name="age" type="number" class="form-control" id="age" placeholder="Please enter your exact age">
        </div>
        <div class="form-group col-6">
        <label for="file">File input</label>
        <input name="fileToUpload" type="file" class="form-control-file" id="file">
        </div>
        <div class="form-group col-12 text-align-center">
            <button name="submit" class="btn btn-primary btn-block">Submit</button>
        </div>
    </div>
</form>
<?php
//end of body
include_once 'footer.php';
?>