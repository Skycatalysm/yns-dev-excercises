<?php
include_once 'head.php';
if (!isset($_SESSION['userSession']['firstName'])) {
    echo '<script src="js/strict_login.js"></script> ';
}
//start of body
?>
<div class="container">
    <div class="text-center p-5">
            <img class="rounded-circle" src="uploads/<?php echo $_SESSION['userSession']['img']; ?>" alt="profile picture" width="140" height="140">
            <h2>
                <?php echo $_SESSION['userSession']['firstName']; ?>
                <?php echo $_SESSION['userSession']['middleName']; ?>
                <?php echo $_SESSION['userSession']['lastName']; ?>
            </h2>
            <p>
                <?php echo $_SESSION['userSession']['email']; ?>
                <span class="text-secondary">(<?php echo $_SESSION['userSession']['age']; ?> years old)</span>
            </p>
    </div>
</div>
<?php
//end of body
include_once 'footer.php';
?>