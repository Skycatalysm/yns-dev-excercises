<?php
include_once 'head.php';
//start of body
?>

<div class="text-center">
<?php
//check if button is submitted
if (isset($_POST['submit'])) {
    ///Check for the submitted values
    if(isset($_POST['firstName'])){
        $firstName = $_POST['firstName'];
    }else{
        $firstName = null;
    } 
    if(isset($_POST['middleName'])){
        $middleName = $_POST['middleName'];
    }else{
        $middleName = null;
    } 
    if(isset($_POST['lastName'])){
        $lastName = $_POST['lastName'];
    }else{
        $lastName = null;
    } 
    if(isset($_POST['email'])){
        $email = $_POST['email'];
    }else{
        $email = null;
    } 
    if(isset($_POST['age'])){
        $age = $_POST['age'];
    }else{
        $age = null;
    } 
    if(isset($_POST['userName'])){
        $userName = $_POST['userName'];
    }else{
        $userName = null;
    } 
    if(isset($_POST['password'])){
        $password = md5($_POST['password']);
    }else{
        $password = null;
    } 


    //Create a variable that will be filled with arrays later
    $userInfo = array();
    
//Check First Name
if ($firstName == null) {
    echo '<h4 class="alert alert-danger">Please provide a first name<h4>';
    $userInfo[] = 'null';
} else {
    if (ctype_alpha($firstName)) {
        /**
         * put First Name as 1st array
         */
        $userInfo[] = $firstName;
    } else {
        $userInfo[] = 'null';
        echo "<div class='alert alert-danger'>First Name: $firstName should only contain alphabet</div>";
    }
}

//Check Middle Name
if ($middleName == null) {
    echo '<h4 class="alert alert-danger">Please provide a middle name<h4>';
    $userInfo[] = 'null';
} else {
    if (ctype_alpha($middleName)) {
        /**
         * put Middle Name as 2nd array
         */
        $userInfo[] = $middleName;
    } else {
        $userInfo[] = 'null';
        echo "<div class='alert alert-danger'>Middle Name: $middleName should only contain alphabet</div>";
    }
}

//Check Last Name
if ($lastName == null) {
    echo '<h4 class="alert alert-danger">Please provide a last name<h4>';
    $userInfo[] = "null";
} else {
    if (ctype_alpha($lastName)) {
        /**
         * put Last Name as 3rd array
         */
        $userInfo[] = $lastName;
    } else {
        $userInfo[] = "null";
        echo "<div class='alert alert-danger'>Last Name: $lastName should only contain alphabet</div>";
    }
}

//Check Email
if ($email == null) {
    echo '<h4 class="alert alert-danger">Please provide an email<h4>';
    $userInfo[] = "null";
} else {
    $regex = '/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/';
    if (preg_match($regex, $email)) {
        /**
         * put Email as 4th array
         */
        $userInfo[] = $email;
    } else {
        $userInfo[] = 'null';
        echo "<div class='alert alert-danger'>Email: $email is not valid</div>";
    }
}

//Check age
if ($age == null) {
    echo '<h4 class="alert alert-danger">Please provide an age<h4>';
    $userInfo[] = 'null';
} else {
    if (ctype_digit($age)) {
        /**
         * put Age as 5th array
         */
        $userInfo[] = $age;
    } else {
        $userInfo[] = 'null';
        echo "<div class='alert alert-danger'>Age: $age should be a number</div>";
    }
}

//Check User Name
if ($userName == null) {
    echo '<h4>Please provide User Name<h4>';
    $userInfo[] = 'null';
} else {
    /**
     * put User name as 6th array
     */
    $userInfo[] = $userName;
}

//Check Password
if ($password == null) {
    echo '<h4 class="alert alert-danger">Please provide a password<h4>';
    $userInfo[] = 'null';
} else {
    /**
     * put Password as 7th array
     */
    $userInfo[] = $password;
}

if (isset($_FILES['fileToUpload']) && $_FILES['fileToUpload']['size'] > 0) {
    //Uploading of image
    $targetDirectory = "uploads/";
    $extension = pathinfo(basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION);
    $imgName = uniqid("img", true) . ".$extension";
    $targetFile = $targetDirectory . $imgName;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            echo 'File is not an image.';
            $uploadOk = 0;
        }
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" && $imageFileType != "jfif") {
        echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo 'Sorry, your file was not uploaded.';
        $userInfo[] = 'null';
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
            $userInfo[] = $imgName;
        } else {
            echo 'Sorry, there was an error uploading your file.';
            array_pop($userInfo);
            $userInfo[] = 'null';
        }
    }
} else {
    echo '<h4 class="alert alert-danger">Please provide an image<h4>';
    $userInfo[] = 'null';

}

//Check each info if it is submitted correctly
foreach ($userInfo as $item) {
    if ($item == "null") {
        $userStatus = 0;
        break;
    }
    $userStatus = 1;
}

//Check if the status is ok, then save
if ($userStatus == 1) {

    //Create query
    $sql = "INSERT INTO user_info (first_name, middle_name, last_name, email, age, username, password, image) VALUES ('$userInfo[0]', '$userInfo[1]', '$userInfo[2]', '$userInfo[3]', '$userInfo[4]', '$userInfo[5]', '$userInfo[6]', '$userInfo[7]')";

    //Connect to database then query
    if ($connect->query($sql) === true) {
        echo '<div class="alert alert-info"><b>Success!</b> You may now Login</div>';
    } else {
        echo '<div class="alert alert-danger"><b>Something went wrong!</b> Please contact your developer. ' . mysqli_error($connect) . '</div>';

    }
    $connect->close();
} else {
    echo '<a class="btn btn-warning" href="#" onclick="history.go(-1)">Failed. Please fix the data given</a>';
}

}
?>
</div>
<?php
//end of body
include_once 'footer.php';
?>