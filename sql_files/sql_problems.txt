SELECT * FROM employee WHERE employee.last_name LIKE 'k%';

SELECT * FROM employee WHERE employee.last_name LIKE '%i';

SELECT CONCAT(first_name,' ',IFNULL(middle_name,''),' ',last_name) AS full_name, hire_date  FROM employee WHERE hire_date BETWEEN '2015-01-1' AND '2015-3-31';

SELECT employee.last_name, boss.last_name AS 'Boss' FROM employee AS employee JOIN employee AS boss ON boss.id = employee.boss_id;

SELECT employee.last_name FROM employee WHERE employee.department_id = (SELECT departments.id FROM departments WHERE departments.name = 'sales') ORDER BY employee.last_name DESC;

SELECT COUNT(employee.middle_name) FROM employee;

SELECT departments.name, COUNT(employee.department_id) AS 'COUNT(D.id'  FROM employee JOIN departments ON employee.department_id = departments.id GROUP BY department_id;

SELECT * FROM employee ORDER BY employee.hire_date ASC LIMIT 1;

SELECT departments.name, departments.id FROM departments WHERE NOT EXISTS (SELECT employee.department_id FROM employee WHERE departments.id = employee.department_id);

SELECT DISTINCT employee.first_name, employee.middle_name, employee.last_name FROM employee JOIN employee_positions GROUP BY employee_positions.employee_id HAVING COUNT(employee_positions.position_id) > 1;